defmodule ModularArithmetic do
  @moduledoc """
  # Modular Arithmetic

  Module provides functions around modular arithmetics.
  """

  @doc """
  Calculates the gcd (greatest common divisor) of two numbers using the euclid
  algorithm, see [wikipedia](https://en.wikipedia.org/wiki/Euclidean_algorithm)
  """
  def euclid(a, b), do: _euclid(a, b)
  """
  Euclid: Base case
  """
  def _euclid(a, 0), do: a
  """
  Euclid: Recursion
  """
  def _euclid(a,b), do: _euclid(b, rem(a,b) )

  @doc """
  Calculates the euler phi function φ(n) = |{k: 1≤k≤n, gcd(k,n) = 1}|  for n.
  """
  def euler_phi(n) when n >= 1 do
    _euler_phi(n)
    |> Enum.count
  end

  @doc """
  Returns the set of numbers fullfilling the euler phi function φ(n) = |{k: 1≤k≤n, gcd(k,n) = 1}|
  """
  def euler_phi_as_list(n) when n >= 1, do: _euler_phi(n)

  @doc """
  Calculates the set of of numbers forming the  {k: 1≤k≤n, gcd(k,n) = 1}
  """
  defp _euler_phi(n) when n>= 1 do
    Stream.iterate(1,&(&1+1))
    |> Stream.filter(&(euclid(&1,n) === 1))
    |> Stream.take_while( &(&1 < n))
    |> Enum.to_list
  end

  @doc """
  Calculates the modular multiplicative inverse of a of m such ax ≡ 1 mod (m)
  """
  def inverse(a,m) do
    :math.pow(a, euler_phi(m)-1)
    |> round
    |> rem(m)
  end
end
