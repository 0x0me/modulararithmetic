defmodule ModularArithmeticTest do
  use ExUnit.Case

  test "Euclid(+/-1071,+/-1029)" do
    assert ModularArithmetic.euclid(1071,1029)    === 21
    assert ModularArithmetic.euclid(-1071,1029)   === 21
    assert ModularArithmetic.euclid(1071,-1029)   === -21
    assert ModularArithmetic.euclid(-1071,-1029)  === -21
  end

  test "euler_phi" do
    assert ModularArithmetic.euler_phi(5)   === 4
    assert ModularArithmetic.euler_phi(6)   === 2
    assert ModularArithmetic.euler_phi(10)  === 4
    assert ModularArithmetic.euler_phi(13)  === 12
    assert ModularArithmetic.euler_phi(15)  === 8
    assert ModularArithmetic.euler_phi(18)  === 6
    assert ModularArithmetic.euler_phi(72)  === 24
    assert ModularArithmetic.euler_phi(128) === 64
  end

  test "euler_phi_as_list" do
    assert ModularArithmetic.euler_phi_as_list(5)  === [1,2,3,4]
    assert ModularArithmetic.euler_phi_as_list(6)  === [1,5]
    assert ModularArithmetic.euler_phi_as_list(10) === [1,3,7,9]
    assert ModularArithmetic.euler_phi_as_list(13) === [1,2,3,4,5,6,7,8,9,10,11,12]
    assert ModularArithmetic.euler_phi_as_list(15) === [1,2,4,7,8,11,13,14]
    assert ModularArithmetic.euler_phi_as_list(18) === [1, 5, 7, 11, 13, 17]
    assert ModularArithmetic.euler_phi_as_list(72) === [1, 5, 7, 11, 13, 17, 19, 23, 25, 29, 31, 35, 37, 41, 43, 47, 49, 53, 55, 59, 61, 65, 67, 71]
  end

  test "inverse" do
    assert ModularArithmetic.inverse(7,10) === 3
    assert ModularArithmetic.inverse(3,11) === 4
  end
end
