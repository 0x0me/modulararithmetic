ModularArithmetic
=================

Functions and helpers for modular arithmetics.

_This modules serves mainly to purposes:_

1. _learning and practicing Elixir and_
2. _supporting to solve exercises and deepening the understanding of the "hardware security" course on [coursera](https://class.coursera.org/hardwaresec-001)._

_It is by no means a sophisticated or even fully tested implementation serving
any specific purpose_
